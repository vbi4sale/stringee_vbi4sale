package com.stringee.cordova;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.content.Context;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.stringee.listener.StatusListener;
import com.vbi.vbi4sales.MainActivity;
import com.vbi.vbi4sales.R;

//import com.vbi.myvbi.MainActivity;

import org.apache.cordova.dialogs.Notification;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by luannguyen on 9/5/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    public static final String NOTIFICATION_CHANNEL_ID = "100001";

    @Override
    public void onNewToken(String token) {
        if (Common.client != null) {
            Common.client.registerPushToken(token, new StatusListener() {
                @Override
                public void onSuccess() {

                }
            });
        }
    }
    public void createNotification(String title, String message)
    {
        String  packageName = getApplication().getPackageName();
        Intent  launchIntent = getApplication().getPackageManager().getLaunchIntentForPackage(packageName);
        /**Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = new Intent(getApplicationContext() , MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(),
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(getApplicationContext());
        mBuilder.setSmallIcon(R.mipmap.icon);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(false)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(0 /* Request Code */, mBuilder.build());
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("Stringee", "Push notification message received");
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if (remoteMessage.getData().size() > 0) {
            String pushFromStringee = remoteMessage.getData().get("stringeePushNotification");
            if (pushFromStringee != null) {
                String data = remoteMessage.getData().get("data");
                Log.d("Stringee", data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    String callStatus = jsonObject.getString("callStatus");
                    if (callStatus != null && callStatus.equals("started")) {
                        if (Common.client == null || !Common.client.isConnected()) {
                            Class mainActivity = null;
                            Context context = getApplicationContext();
                            String  packageName = context.getPackageName();
                            Intent  launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
                            String  className = launchIntent.getComponent().getClassName();
                            try {
                                //loading the Main Activity to not import it in the plugin
                                mainActivity = Class.forName(className);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Intent openActivityIntent = new Intent(context, mainActivity);


                            openActivityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(openActivityIntent);
//                            Intent intent = new Intent(this, MainActivity.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
               String title = remoteMessage.getData().get("title");
                String body = remoteMessage.getData().get("body");
                createNotification(title,body);
//               Drawable icon = this.getPackageManager().getApplicationIcon("com.example.samplepackagename");
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    //deprecated in API 26
                    v.vibrate(500);
                }
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                r.play();
            }
        }
    }
}
